var express    	= require("express");
var app        	= express();
var bodyParser 	= require("body-parser");
var request 	= require("request");
var cheerio 	= require("cheerio");
var redis 		= require("redis");
var config		= require("./redis-config.js");

var client = redis.createClient();

client.auth(config.password, function(err){
	if(err){
		console.log(err);
	}
});

client.on("connect", function(){
	console.log("connected to redis!");
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var j = request.jar();
var port = process.env.PORT || 8080;

app.get("/", function(req, res) {
    res.send("visit /cardBalance");   
});

app.get("/cardBalance", function(req, res){
	if(req.query.cardNumber){
		client.get(req.query.cardNumber, function(err, reply){
			if(reply == null){
				request({url: "https://pilet.elron.ee/Account/Login?cardNumber="+req.query.cardNumber, jar: j}, function(error, response, html){
					j = request.jar();
					if(!error){
						var $ = cheerio.load(html);
	
						var cardBalance = $("#content > section > section > div.centered-block > div:nth-child(2) > fieldset > input").val();
						if(cardBalance){
							client.set(req.query.cardNumber, cardBalance);
							client.expire(req.query.cardNumber, 300);
								
							res.json(formMessage({ cardnumber: req.query.cardNumber, cardbalance: cardBalance, type: "direct" }));	
						}else{
							res.json(formMessage({ message: "Card number must be between 11 and 16 digits", cardnumber: req.query.cardNumber }));
						}
					}else{
						res.json(formMessage({ message: "Failed to contact pilet.elron.ee (" + response.statusCode + ")", cardnumber: req.query.cardNumber }));
					}
				});
			}else{
				client.ttl(req.query.cardNumber, function(err2, reply2){
					res.json(formMessage({ cardnumber: req.query.cardNumber, cardbalance: reply, type: "cached", timer: reply2}));
				});
			}
		});
	}else{
		res.json(formMessage({ result: "error", message: "Have you filled the GET parameter 'cardNumber' ?" }));
	}
});

function formMessage(x){
	return {
		result: x.message == null ? "success" : "error",
		message: x.message || null,
		cardnumber: x.cardnumber || null,
		cardbalance: x.cardbalance || null,
		type: x.type || null,
		timer: x.timer|| null
	};
}

app.listen(port);